package anhpt28.selenium.demo.common;

public class XpathConstants {
	public static final String FACEBOOK_TXT_EMAIL = "//*[@id=\"email\"]";
	public static final String FACEBOOK_TXT_PASSWORD = "//*[@id=\"pass\"]";
	public static final String FACEBOOK_BUTTON_LOGIN = "//*[@id=\"loginbutton\"]";
	public static final String FACEBOOK_BUTTON_USERNAV = "//*[@id=\"userNavigationLabel\"]";
	public static final String FACEBOOK_BUTTON_LOGOUT = "//span[@class='_54nh'][contains(.,'Đăng xuất')]";
	
	public static final String GURU99_TXT_INPUTFILE = "//*[@id=\"uploadfile_0\"]";
	public static final String GURU99_CHECKBOX_TERMS = "//*[@id=\"terms\"]";
	public static final String GURU99_BUTTON_SUBMIT = "//*[@id=\"submitbutton\"]";
	
	public static final String GURU99_GENERATE_ACC_TXT_EMAIL = "/html/body/form/table/tbody/tr[5]/td[2]/input";
	public static final String GURU99_GENERATE_ACC_BUTTON_SUBMIT = "/html/body/form/table/tbody/tr[6]/td[2]/input";
	
	public static final String GURU99_HOME_TXT_USER = "/html/body/table/tbody/tr[4]/td[2]";
	public static final String GURU99_HOME_TXT_PASSWORD = "/html/body/table/tbody/tr[5]/td[2]";
	
	public static final String GURU99_LOGIN_TXT_USER = "/html/body/form/table/tbody/tr[1]/td[2]/input";
	public static final String GURU99_LOGIN_TXT_PASSWORD = "/html/body/form/table/tbody/tr[2]/td[2]/input";
	public static final String GURU99_LOGIN_BUTTON_SUBMIT = "/html/body/form/table/tbody/tr[3]/td[2]/input[1]";
	
	public static final String GURU99_MANAGER_TXT_USERID = "/html/body/table/tbody/tr/td/table/tbody/tr[3]/td";
	public static final String GURU99_MANAGER_NAV_AGILE = "//*[@id=\"navbar-brand-centered\"]/ul/li[4]/a";
	
	public static final String GURU99_AGILE_TXT_USER_ACCESS = "/html/body/div[4]/ol/li[1]";
	public static final String GURU99_AGILE_TXT_PASSWORD_ACCESS = "/html/body/div[4]/ol/li[2]";
	public static final String GURU99_AGILE_TXT_USER = "/html/body/form/table/tbody/tr[1]/td[2]/input";
	public static final String GURU99_AGILE_TXT_PASSWORD = "/html/body/form/table/tbody/tr[2]/td[2]/input";
	public static final String GURU99_AGILE_BUTTON_SUBMIT = "/html/body/form/table/tbody/tr[3]/td[2]/input[1]";
	public static final String GURU99_AGILE_TXT_LOGIN_WELCOME = "/html/body/table/tbody/tr/td/table/tbody/tr[2]/td/marquee";
	
	public static final String TOOLSQA_BUTTON_SHARE_TWEET = "//*[@id=\"content\"]/div[3]/div[2]/a[2]";
	public static final String TOOLSQA_FRAME1_TXT_FIRSTNAME = "//*[@id=\"content\"]/div[1]/div/div/div/div[2]/div/form/fieldset/div[8]/input";
	public static final String TOOLSQA_FRAME1_TXT_LASTNAME = "//*[@id=\"content\"]/div[1]/div/div/div/div[2]/div/form/fieldset/div[11]/input";
	public static final String TOOLSQA_FRAME1_RADIO_SEX_MALE = "//*[@id=\"sex-0\"]";
	public static final String TOOLSQA_FRAME1_RADIO_1_YEAR_EXP = "//*[@id=\"exp-0\"]";
	public static final String TOOLSQA_FRAME1_TXT_DATE = "//*[@id=\"datepicker\"]";
	public static final String TOOLSQA_FRAME1_RADIO_PRO = "//*[@id=\"profession-1\"]";
	
	public static final String TWITTER_TXT_ACC = "//*[@id=\"username_or_email\"]";
	public static final String TWITTER_TXT_PASSWORD = "//*[@id=\"password\"]";
	public static final String TWITTER_BUTTON_LOGIN_AND_TWEET = "//*[@id=\"update-form\"]/div[3]/fieldset[2]/input";
	
	public static final String CEZERIN_NAV_FOOTWEAR = "//*[@id=\"app\"]/header/div/div[2]/ul/li[2]/div/a";
	public static final String CEZERIN_NAV_FOOTWEAR_TRAINERS = "//*[@id=\"app\"]/header/div/div[2]/ul/li[2]/ul/li[1]/ul/li[1]/div/a";
	public static final String CEZERIN_SIZE_11 = "//*[@id=\"app\"]/section[2]/div/div/div[1]/div/div[2]/div[2]/div/div[2]/div[2]/label[3]/input";
	public static final String CEREZIN_SELECT_SORT = "//*[@id=\"app\"]/section[2]/div/div/div[2]/div[1]/div[2]/div/div[2]/span/select";
	public static final String CEREZIN_SORT_FIRST_PRODUCT = "//*[@id=\"app\"]/section[2]/div/div/div[2]/div[2]/div[1]";
	public static final String CEREZIN_PRODUCT_QUANTITY = "//*[@id=\"app\"]/section[1]/div/div/div[2]/div/div[3]/input";
	public static final String CEREZIN_BUTTON_ADD_TO_CART = "//*[@id=\"app\"]/section[1]/div/div/div[2]/div/div[4]/button";
	public static final String CEREZIN_BUTTON_GO_TO_CHECKOUT = "//*[@id=\"app\"]/header/div/div[1]/div[3]/div[2]/div/a";
	public static final String CEREZIN_SELECT_QUANTITY = "//*[@id=\"app\"]/section[1]/div/div/div[1]/div/div[2]/div[2]/div[2]/span[2]/select";
}
