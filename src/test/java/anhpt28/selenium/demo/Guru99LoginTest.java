//package anhpt28.selenium.demo;
//
//import org.apache.log4j.Logger;
//import org.openqa.selenium.WebDriver;
//import org.testng.Assert;
//import org.testng.annotations.AfterClass;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;
//
//import anhpt28.selenium.demo.log.LogFactory;
//import anhpt28.selenium.demo.model.Guru99BankAgileProjectPage;
//import anhpt28.selenium.demo.model.Guru99BankHomePage;
//import anhpt28.selenium.demo.model.Guru99BankManagerHomePage;
//import anhpt28.selenium.demo.utils.WebDriverUtils;
//
//public class Guru99LoginTest {
//
//  private static final String USERID_INPUT_TEXT = "mngr136913";
//  private static final String PASSWORD_INPUT_TEXT = "bath1234@";
//  private WebDriver driver;
//  private WebDriverUtils webDriverUtils;
//  private static final Logger LOGGER = LogFactory.getLogger();
//
//  @BeforeClass
//  public void setup() {
//    this.webDriverUtils = WebDriverUtils.getInstance();
//    this.driver = this.webDriverUtils.getChromeWebDriver();
//  }
//  
//  @Test
//  public void loginGuru99TestCase() throws InterruptedException {
//    Guru99BankHomePage guru99BankHomePage = new Guru99BankHomePage(this.driver);
//    guru99BankHomePage.goToPage();
//    Thread.sleep(1000);
//    guru99BankHomePage.login(USERID_INPUT_TEXT, PASSWORD_INPUT_TEXT);
//    Thread.sleep(1000);
//    Guru99BankManagerHomePage guru99BankManagerHomePage = new Guru99BankManagerHomePage(driver);
//    Assert.assertTrue(guru99BankManagerHomePage.verifyUserId(USERID_INPUT_TEXT));
//    guru99BankManagerHomePage.clickAgileProjectMenu();
//    Thread.sleep(1000);
//    Guru99BankAgileProjectPage guru99BankAgileProjectPage = new Guru99BankAgileProjectPage(driver);
//    guru99BankAgileProjectPage.loginWithAccessAccount();
//    Thread.sleep(5000);
//    Assert.assertTrue(guru99BankAgileProjectPage.verifyLoginSuccess());
//  }
//  
//  @AfterClass
//  public void close() {
//    this.driver.close();
//    this.driver.quit();
//  }
//}
