package anhpt28.selenium.demo.log;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class LogFactory {

  /**
   * Get logger
   * 
   * @return logger
   */
  public static Logger getLogger() {
    Properties props = new Properties();
    try {
      props.load(LogFactory.class.getClassLoader().getResourceAsStream("log4j.properties"));
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    PropertyConfigurator.configure(props);
    Logger logger = Logger
        .getLogger(Thread.currentThread().getStackTrace()[2].getClassName());
    return logger;
  }
}
