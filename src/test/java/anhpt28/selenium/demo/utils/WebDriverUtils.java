package anhpt28.selenium.demo.utils;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class WebDriverUtils {

	private static WebDriverUtils instance;

	private WebDriverUtils() {
	  }

	public static WebDriverUtils getInstance() {
		if (instance == null) {
			synchronized (WebDriverUtils.class) {
				instance = new WebDriverUtils();
			}
		}
		return instance;
	}
	
	public ChromeDriver getChromeWebDriver() {
		System.setProperty("webdriver.chrome.driver", "D:\\FPT\\Course\\Automation\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("disable-geolocation");
		options.addArguments("disable-notifications");
		return new ChromeDriver(options);
	}
	
	public FirefoxDriver getFirefoxWebDriver() {
		System.setProperty("webdriver.gecko.driver", "D:\\FPT\\Course\\Automation\\geckodriver.exe");
		FirefoxOptions options = new FirefoxOptions();
		options.addPreference("dom.webnotifications.enabled", false);
		options.addPreference("app.update.enabled", false);
		options.addPreference("geo.enabled", false);
		return new FirefoxDriver(options);
	}
	
	public InternetExplorerDriver getIEWebDriver() {
		System.setProperty("webdriver.ie.driver", "D:\\FPT\\Course\\Automation\\IEDriverServer.exe");
		return new InternetExplorerDriver();
	}
}
