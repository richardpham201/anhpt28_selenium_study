package anhpt28.selenium.demo.model;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Guru99BankAgileProjectPage {

  private WebDriver driver;
  private static final String USER_ACCESS_TEXT_XPATH = "//li[contains(.,'UserID :')]";
  private static final String PASSWORD_ACCESS_TEXT_XPATH = "//li[contains(.,'Password :')]";
  private static final String USER_INPUT_XPATH = "//input[@name=\"uid\"]";
  private static final String PASSWORD_INPUT_XPATH = "//input[@name=\"password\"]";
  private static final String LOGIN_BUTTON_XPATH = "//input[@name=\"btnLogin\"]";
  private static final String MARQUEE_XPATH = "//marquee";

  public Guru99BankAgileProjectPage(WebDriver driver) {
    this.driver = driver;
  }

  public void loginWithAccessAccount() {
    WebElement userIdOutputTxt = this.driver.findElement(By.xpath(USER_ACCESS_TEXT_XPATH));
    String userAccess = userIdOutputTxt.getText().replace("UserID : ", "");
    WebElement passwordOutputTxt = this.driver.findElement(By.xpath(PASSWORD_ACCESS_TEXT_XPATH));
    String password = passwordOutputTxt.getText().replace("Password : ", "");

    WebElement accountInputField = this.driver.findElement(By.xpath(USER_INPUT_XPATH));
    accountInputField.sendKeys(userAccess);

    WebElement passwordInputField = this.driver.findElement(By.xpath(PASSWORD_INPUT_XPATH));
    passwordInputField.sendKeys(password);

    WebElement loginButton = this.driver.findElement(By.xpath(LOGIN_BUTTON_XPATH));
    loginButton.click();
  }

  public boolean verifyLoginSuccess() {
    WebElement marquee = this.driver.findElement(By.xpath(MARQUEE_XPATH));
    String marqueeTxt = marquee.getText();
    return "Welcome To Customer's Page of Guru99 Bank".equals(marqueeTxt);
  }

}
