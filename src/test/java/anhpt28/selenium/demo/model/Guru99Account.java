package anhpt28.selenium.demo.model;

public class Guru99Account {
  private String user;
  private String password;

  /**
   * @return the user
   */
  public String getUser() {
    return user;
  }

  /**
   * @param user
   *          the user to set
   */
  public void setUser(String user) {
    this.user = user;
  }

  /**
   * @return the password
   */
  public String getPassword() {
    return password;
  }

  /**
   * @param password
   *          the password to set
   */
  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  public String toString() {
    return "Guru99Account [user=" + user + ", password=" + password + "]";
  }

  /**
   * @param user
   * @param password
   */
  public Guru99Account(String user, String password) {
    super();
    this.user = user;
    this.password = password;
  }

  /**
   * 
   */
  public Guru99Account() {
    super();
  }

}
